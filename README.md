# CarCar

Team:

* Stephen - Service
* Andrew - Sales

## How to run this App

* Clone the repository
* In your desired project directory use: `git clone <<link of cloned repository>>`
* `cd project-beta`
* `docker volume create beta-data`
* `docker-compose build`
* `docker-compose up`
* Project can be viewed at http://localhost:3000/

## Diagram
![Alt text](Diagram.png)

## Service microservice Information

The goal of this project is to create a functional website that can create appointments, customers, automobiles and technicians while alos being able to keep track of sales and service history and create new ones. Both sales and services require models to set up. For sales, there are sales, customer, and salesperson. For services, there are appointment and technician. Both have AutomobileVO in models as well to keep track of the VIN and sold automobiles. Each require forms that can be created so there are views folder for both sales and services that allows user to create forms like making an appointment.


| Functionality | Detail |
| ----------- | ----------- |
| Add Technician | A link to add technician |
| List Technicians | Be able to have a list of current technicians |
| Create Appointment | Enter vin, customer name, time, assigned tech, and reason |
| List Appointment | List appointments including the VIN, customer name, reaseon |
| List Appointment (Specialty) | Each appointment shows VIP and Appointment status |
| Service History | Shows the history of appointments (old and new) |

To keep track of service appointments for automobiles and their owners, the automobile service
has 3 different models:

1. Technician Model - Information of Technician
2. AutomobileVO Model - Takes in a vin and sold field
3. Appointment Model - Appointment information (Time, Reason, status, vin, customer info, and technician assinged)

To kick off this project, Docker needs to be installed correctly to run localhost and run this command in terminal

Note that if using macOS, it will show a warning about an environment variable named OS being missing. Please ignore.

## Endpoints for Service

The automobile service needs to have theses API endpoint listed below:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET |  http://localhost:8080/api/technicians/
| reate a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/
| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/
| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel/
| Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish/

## Sales microservice

Models:

- AutomobileVO: Takes in a vin and sold field
- SalesPerson: Takes in first, last names, and an employee id
- Customer: Takes in first, last names, address, and phone number
- Sale: Takes in price, Customer, SalesPerson, and AutomobileVO

Functionalities:

- Create:
    - Sales People
    - Customers
    - Sales

- List:
    - Sales People
    - Customers
    - Sales
    - Sales History of specific Sales person

- Delete(via insomnia):
    - Sales People
    - Customers
    - Sales


## Sales URLs

Sales People List: http://localhost:3000/salespeople/list/

Sales People Create: http://localhost:3000/salespeople/create/

Customer List: http://localhost:3000/customers/list/

Customer Create: http://localhost:3000/customer/create/

Sales List: http://localhost:3000/sales/list/

Sale Create: http://localhost:3000/sales/create/


Sales History: http://localhost:3000/sales/history/

## Sales Ports and CRUD

<details>
    <summary>Sales People</summary>
    <details>
        <summary>GET a list of Sales People</summary>

![Sales People](SalesPeopleList.png)
    </details>
    <details>
        <summary>POST create a new Sales Person</summary>

![Sales People](SalesPersonCreate.png)
    </details>
    <details>
        <summary>DELETE a Sales Person</summary>

![Sales People](DeleteSalesPerson.png)
    </details>
</details>

<details>
    <summary>Customers</summary>
    <details>
        <summary>GET a list of Customers</summary>

![Customers](CustomerList.png)
    </details>
    <details>
        <summary>POST create a new Customer</summary>

![Customers](CustomerCreate.png)
    </details>
    <details>
        <summary>DELETE a Customer</summary>

![Customers](CustomerDelete.png)
    </details>
</details>

<details>
    <summary>Sales</summary>
    <details>
        <summary>GET a list of Sales</summary>

![Sales](SalesList.png)
    </details>
    <details>
        <summary>POST create a new Sale</summary>

![Sales](SaleCreate.png)
    </details>
    <details>
        <summary>DELETE a Sale</summary>

![Sales](SaleDelete.png)
    </details>
</details>

## Inventory Ports and CRUD

<details>
    <summary>Manufacturers</summary>
    <details>
        <summary>GET a list of Manufacturers</summary>

![Manufacturers](ManufacturersList.png)
    </details>
    <details>
        <summary>POST create a new Manufacturer</summary>

![Manufacturers](ManufacturerCreate.png)
    </details>
    <details>
        <summary>DELETE a Manufacturer</summary>

![Manufacturers](ManufacturerDelete.png)
    </details>
</details>

<details>
    <summary>Models</summary>
    <details>
        <summary>GET a list of Models</summary>

![Models](ModelList.png)
    </details>
    <details>
        <summary>POST create a new Model</summary>

![Models](ModelCreate.png)
    </details>
    <details>
        <summary>DELETE a Model</summary>

![Models](ModelDelete.png)
    </details>
</details>

<details>
    <summary>Automobiles</summary>
    <details>
        <summary>GET a list of Automobiles</summary>

![Automobiles](AutoList.png)
    </details>
    <details>
        <summary>POST create a new Automobile</summary>

![Automobiles](AutoCreate.png)
    </details>
    <details>
        <summary>DELETE an Automobile</summary>
![Automobiles](AutoDelete.png)
    </details>
</details>

## Value Object

* AutomobileVO
    - The Value Object for the Sales microservice is the AutomobileVO. It accesses the Inventory API to pull the list of automobiles inside of it for use in the sales microservice. With this we can create new sales or list sales with the automobile data found within the Inventory.
