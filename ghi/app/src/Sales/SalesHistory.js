import { useEffect, useState } from "react";

function SalesHistory() {
    const [salesperson, setSalesPerson] = useState('');
    const [salespeople, setSalesPeople] = useState([]);
    const [sales, setSales] = useState([]);

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    };

    const getSales = async () => {
        const salesUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(salesUrl);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    };

    const getSalesPeople = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    };

    useEffect(() => {
        getSales();
        getSalesPeople();
    }, []);


    return (
        <div>
            <h1>Sales History</h1>
            <div className="mb-3">
                <select value={salesperson} required onChange={handleSalesPersonChange} name="salesperson" id="salesperson" className="form-select">
                    <option value="" >Select Sales Person</option>
                    {salespeople.map((salesperson) => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter((sale) => sale.salesperson.employee_id === salesperson).map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>
                                    {sale.salesperson.first_name} {sale.salesperson.last_name}
                                </td>
                                <td>
                                    {sale.customer.first_name} {sale.customer.last_name}
                                </td>
                                <td>
                                    {sale.automobile.vin}
                                </td>
                                <td>
                                    {sale.price}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default SalesHistory;
