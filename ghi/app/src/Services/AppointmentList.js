import React, { useEffect, useState } from 'react';

export default function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const handleFinish = async (id) => {
        const appointURL = `http://localhost:8080/api/appointments/${id}/finish/`;
        const appointConfig = {
            method: 'PUT',
            body: JSON.stringify({status: 'finished'}),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const appointFinished = await fetch(appointURL, appointConfig)
        if (appointFinished.ok) {
            fetchData();
        }
    };

    const handlCancel = async (id) => {
        const appointURL = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const appointConfig = {
            method: 'PUT',
            body: JSON.stringify({status: 'canceled'}),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const appointFinished = await fetch(appointURL, appointConfig)
        if (appointFinished.ok) {
            fetchData();
        }
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th> Date/Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.vin}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.vip }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date_time }</td>
                            <td>{ appointment.technician.first_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                <div>
                                    <button onClick={() => handlCancel(appointment.id, "canceled")} type="button" className="btn-cancel" >Cancel</button>{' '}
                                    <button onClick={() => handleFinish(appointment.id, "finished")} type="button" className="btn-finish" >Finish</button>{' '}
                                </div>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}
