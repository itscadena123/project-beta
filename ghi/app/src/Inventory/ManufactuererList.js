import React, { useEffect, useState } from 'react';

export default function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
          <h1 className="mb-3 text-center">Vehicle Manufacturers</h1>
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Name</th>
              </tr>
            </thead>
            <tbody>
              {manufacturers.map((manufacturer) => {
                return (
                  <tr key={manufacturer.id}>
                    <td>{manufacturer.name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );

}
