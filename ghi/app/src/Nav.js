import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Salespeople</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/salespeople/list/">View all Salespeople</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/create/">Add New Salesperson</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Customers</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/customers/list/">View all Customers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customer/create/">Add New Customer</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/sales/list/">View all Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/history/">View Salesperson History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/create/">Add New Sale</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Manufacturer</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturer/list/">View all Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturer/create/">Add New Manufacturer</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Models</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/models/list/">View all Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/create/">Add New Models</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Automobiles</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/automobile/list/">View all Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobile/create/">Add New Automobile</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Technicians</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/technicians/list/">View all Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/create/">Create New Technician</NavLink></li>
              </ul>
            </li>

            <li className="nav-item dropdown ms-2">
              <a className="btn btn-primary dropdown-toggle" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">Appointments</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointments/list">View all Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/create/">Add New Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history/">Appointment History</NavLink></li>
              </ul>
            </li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
