from django.urls import path
from .views import (
    api_list_technicians,
    api_canceled_appointment,
    api_appointment,
    api_finished_appointment,
    api_list_appointments,
    api_technicians,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", api_technicians, name="api_technicians"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path(
        "appointments/<int:id>/",
        api_appointment,
        name="api_appointment",
    ),
    path(
        "appointments/<int:id>/cancel/",
        api_canceled_appointment,
        name="api_cancelled_appointment",
    ),
    path(
        "appointments/<int:id>/finish/",
        api_finished_appointment,
        name="api_finished_appointment",
    ),

]
