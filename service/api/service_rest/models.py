from django.db import models

# Create your models here.

class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50, unique=True)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=25)
    sold = models.BooleanField(default=False)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=150)
    status = models.CharField(max_length=50, default="Scheduled")
    vin = models.CharField(max_length=25)
    customer = models.CharField(max_length=50)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
